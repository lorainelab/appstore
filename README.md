## An App Store for IGB

This repository contains the code for the IGB App Store Web application, deployed at the [BioViz.org Web site](https://bioviz.org). 

## Set-up

To stand up a local copy of the app store, follow instructions in [this Google doc](https://docs.google.com/document/d/1_9C03q6TD5wjLqfVLKsuDsIEQ4-qrS0JhONO5VBOwaA/edit?usp=sharing).

For information on how to deploy this site using Amazon Web Service infrastructure and ansible playbooks, see [this repository](https://bitbucket.org/lorainelab/appstore-playbooks).

## Credits

This code base started as a fork of the Cytoscape App Store repository's `wip` (work-in-progress) branch, which became our main or master branch of development. 

Several hundred commits later, the code is now very different, thanks to the efforts of many developers, including:

* Ann Loraine
* Sneha Ramesh Watharkar 
* Pranav Sanjay Tambvekar
* Kiran Korey 
* Narendra Kumar Vankayala
* Riddhi Jagdish Patil
* Sai Charan Reddy Vallapureddy
* Srishti Tiwari 
* Sameer Shanbhag
* Noor Zahara
* Chirag Shetty
* Philip Badzuh